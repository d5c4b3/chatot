# Setup

## Prerequisites
You will need 
* `node.js` and `npm` to run and install the application.
* A discord server that you can invite the bot to for testing.
* A Mysql or MariaDB server instance. (Debian's mysql package is actually just MariaDB)

Use git clone to clone this repo, then follow the steps below.
**Don't put sensitive info into `auth.example.json`**. Copy this file to `auth.json` then add the passwords.

## Discord setup
1. Go to http://discordapp.com/developers/applications/me
2. Click "Create an Application", give the bot a name, then save changes
3. On the right menu click "Bot" > "Add Bot"
4. Click "Click to Reveal Token", **This is a private authentication token that should not be shared**.
5. Copy this value into the auth.json file where it says "Enter token here"
6. On the right menue click "General Information" find the Client ID
7. Visit the URL, replacing CLIENTID with your client id. `https://discordapp.com/oauth2/authorize?&client_id=CLIENTID&scope=bot&permissions=8`. This will allow you to add the bot to your testing server.

## Database & Code setup
1. Update the auth.json file with your database information
2. Run the schema.sql then pokedex_data.sql in your database (`mysql -u username -p database < schema.sql`)
3. Run `npm install --dev` to install the code dependencies
4. Run the bot with `node main.js` If there are no errors you should see that your bot is online in discord. You can test the bot with `!ping`. The bot should reply with "🎶Chatot Cha🎶 Pong!"
