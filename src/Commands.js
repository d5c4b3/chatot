var Database = require('./Database.js');
var Raid = require('./Raid.js');
var db = new Database();

var Commands = function(client, logger) {
    // Help messages for the help command
    var helpMsgs = {
      ping: 'replies with pong',
      wild: '!wild <pokemon> [info] \nCalls out a pokemon as being near the building',
      want: '!want <pokemon> || !want list \nWhenever a raid or pokemon you want is called out you\'ll get a notification.',
      unwant: '!unwant <pokemon> You\'ll no longer get notifications for <pokemon>',
      raid: '!raid <pokemon> <timeleft> || !raid egg <level> <timeleft> \nStarts a raid in the current channel.',
      coming: '!coming \nMarks you as attending the current raid',
      cancel: '!cancel \nUndoes a !coming command'
    };
    //Adding the help commands message
    helpMsgs['help'] = 'tmp';
    helpMsgs['help'] = "Calling !help [cmd] will give you detailed information about each command. Here are the commands I currently know: \n" + Object.keys(helpMsgs).sort().join(', ');

  var raids = new Map();


  //!ping
  this.ping = function(message, args) {
    message.channel.send("🎶Chatot Cha🎶 Pong!");
  }

  //!coming
  this.coming = function(message, args) {
    db.isRaidChannel(message.channel.id)
      .then((isRaidChannel) => {
        if (!isRaidChannel) {
          message.channel.send('🎶CHA!🎶 This is not a raid channel');
          return;
        }

        var raid = raids.get(message.channel.id);
        if (!raid) {
          message.channel.send('🎶 Cha! There is not currently a raid going on.');
          return;
        }

        raid.addComing(message.author);
        message.channel.send('🎶 Cha! '+message.author+' is coming to the raid. (You can use !cancel to undo this)');

      })
      .catch((err) => {
        sendError(message, err);
      });
  }

  //Alias for !coming
  this.c = function(message, args) {
    this.coming(message, args);
  }

  //!cancel
  this.cancel = function(message, args) {
    db.isRaidChannel(message.channel.id)
      .then((isRaidChannel) => {
        if (!isRaidChannel) {
          message.channel.send('🎶CHA!🎶 This is not a raid channel');
          return;
        }

        var raid = raids.get(message.channel.id);
        if (!raid) {
          message.channel.send('🎶 Cha! There is not currently a raid going on.');
          return;
        }

        raid.removeComing(message.author);
        message.channel.send('🎶 Cha! '+message.author+' is no longer coming to the raid.');

      })
      .catch((err) => {
        sendError(message, err);
      });
  }

  //Alias for !cancel
  this.x = function(message, args) {
    this.cancel(message, args);
  }

  //!raid <pokemon> <timeleft> || !raid egg <level> <timeleft>
  this.raid = function(message, args) {
    if (!(args.length == 1 || args.length == 2 || args.length == 3)) {
      this.help(message, 'raid'); return;
    }

    //!raid clear
    if (args[0] == 'clear') {
      if (!message.channel.permissionsFor(message.member).has('MANAGE_CHANNELS', true)) return;
      let raid = raids.get(message.channel.id);
      if (raid) {
        raid.clearTimeouts();
      }
      raids.delete(message.channel.id);
      message.channel.send('🎶Chatot!🎶 Any ongoing raid in this channel has been cleared');
      return;
    }

    db.isRaidChannel(message.channel.id)
      .then((isRaidChannel) => {
        if (!isRaidChannel) {
          message.channel.send('🎶CHA!🎶 This is not a raid channel');
          return;
        }

        //!raid status
        if (args[0] == 'status') {
          var raid = raids.get(message.channel.id);
          if (!raid) {
            message.channel.send('🎶 Cha.. There is not an active raid');
            return;
          }

          if (raid.type == Raid.EGG) {
            message.channel.send(`🎶 Cha tot! 🎶 There is a level ${raid.level} egg that is hatching at ${raid.fTimeEnd()}`);
            return;
          }

          if (raid.type == Raid.POKEMON) {
            var msg = `🎶 Cha tot! 🎶 There is a ${raid.pokemon} raid. `;
            if (raid.groupTime !== false) {
              msg += `The group will be going out at ${raid.fGroupTime()}.`;
            } else {
              msg += `The group may have already gone out.`;
            }
            message.channel.send(msg);
            return;
          }

        }

        if (raids.has(message.channel.id) && raids.get(message.channel.id).type != Raid.EGG) {
          message.channel.send('Chatot 🎶 There is already a raid going on.');
          return;
        }

        if (args[0] == 'egg') {
          raidEgg(message, args[1], args[2]);
        } else {
          raidPoke(message, args[0], args[1]);
        }

      })
      .catch((err) => {
        sendError(message, err);
      });
  }

  //Helper function for !raid
  //Creates a raid based on level and timeLeft in minutes
  function raidEgg(message, level, timeLeft) {
    if (level < 1 || level > 5) {
      message.channel.send('🎶 Tot! Level must be between 1 and 5');
      return;
    }

    if (timeLeft < 0) {
      message.channel.send('🎶 Cha! For an already hatched egg use !raid <pokemon> <timeleft>');
      return;
    }

    var raid = new Raid(message, Raid.EGG, timeLeft, level, undefined);
    raids.set(message.channel.id, raid);

    message.channel.send('🎶 Chatot! 🎶 '+message.author+' Thank you for reporting this raid. You will recieve a notification when the egg has hatched');

    raid.onEnd(() => {
      message.channel.send('🎶Cha!🎶'+message.author+' The egg has hatched! Use !raid <pokemon> <timeleft> to report the pokemon');
    });

  }

  function raidPoke(message, poke, timeLeft) {
    if (timeLeft < 0) {
      message.channel.send('🎶 Cha! This raid has already ended');
      return;
    }

    db.getPokemonByName(poke)
      .then((pokemon) => {
        if (pokemon === undefined) {
          message.channel.send('Cha!🎶 Ive never heard of that pokemon.');
          return;
        }

        var raid = new Raid(message, Raid.POKEMON, timeLeft, undefined, pokemon.name);

        if (raids.has(message.channel.id)) {
          oldRaid = raids.get(message.channel.id);
          if (oldRaid.type == Raid.EGG) {
            var now = new Date();
            //egg hasn't hatched yet
            if (oldRaid.timeEnd > now) {
              //There is a conflict
              message.channel.send('Tot... It seems there is a conflict. Please contact a moderator to clear the current egg timer if you think this is an error.');
              return;
            }

            //this raid is the hatching of the old raid
            if (oldRaid.timeHatch > raid.timeBegan) {
              //TODO add hatch function to Raid
              raid.coming = oldRaid.coming;
              raid.level = oldRaid.level;
            } else {
              //raid is old delete it.
              oldRaid.clearTimeouts();//just in case
              raids.delete(message.channel.id);
            }
          }
        }

        raids.set(message.channel.id, raid);

        var groupStr = '';

        if (raid.timeLeft > Raid.TIME_BEFORE_END) {
          groupStr = 'We will be going out at '+raid.fGroupTime()+'. Please use !coming to coordinate attendance';
        } else {
          groupStr = 'Since there is less than 15 minutes left those that want to go should go now. You can still use !coming to coordinate attendance';
        }

        message.channel.send('🎶 Chatot! 🎶 '+raid.getRoleMention()+' '+message.author+' A raid is here! '+groupStr);

        raid.onEnd(() => {
          raids.delete(raid.channelID);
          message.channel.send('🎶Cha!🎶 The raid has ended.');
        });

        if (raid.hasGroup()) {
          raid.onGroupTime(() => {
            message.channel.send('🎶 Cha Tot! 🎶 '+raid.getRoleMention()+' '+raid.getComingMentions()+' Unless other plans have been made it\'s time to go out now!');
          });
        }

      })
      .catch((err) => {
        sendError(message, err);
      });
  }

  //!addRaidChannel
  this.addRaidChannel = function(message, args) {
    //Make sure the user can edit this channel
    if (!message.channel.permissionsFor(message.member).has('MANAGE_CHANNELS', true)) return;

    db.addRaidChannel(message.guild.id, message.channel.id)
      .then((data) => {
        message.channel.send('🎶Chatot🎶 This channel has been marked as a raid channel');
      })
      .catch((err) => {
        sendError(message, err);
      });
  }

  //!wild pokemon [a location description]
  this.wild = function(message, args) {
    if (args.length < 1) { this.help(message, 'wild'); return; }
    var noWantsMsg = '🎶 Cha.. Thanks for the call out. Too bad no one wants ';
    db.getPokemonByName(args[0])
      .then((pokemon) => {
        var role = message.guild.roles.find(val => val.name === pokemon.name);
        console.log(role);
        if (!role || role.members.size == 0) {
          message.channel.send(noWantsMsg + pokemon.name);
          return;
        }

        message.channel.send("🎶 Cha!!! 🎶 " +role+" "+message.author+" says there is a "+pokemon.name+" nearby.");
      })
      .catch((err) => {
        sendError(message, err);
      });
  }

  //!want [form] <pokemon> || !want list
  this.want = function(message, args) {
    if (args.length < 1) { this.help(message, 'want'); return; }
    //!want list
    if (args[0] == 'list') {
      var roleNames = message.member.roles.array().map(a => a.name);
      db.filterPokeRoles(roleNames)
        .then((data) => {
          var msg = 'Chatot Cha🎶 ';

          if (data.length == 0) {
            msg += 'You don\'t want anything right now. use !want <pokemon> to add something.';
            message.channel.send(msg);
            return;
          }

          var roles = data.map(a => a.name).join(', ');

          msg += message.author + ' wants ' + roles;
          message.channel.send(msg);
        })
        .catch((err) => {
          sendError(message, err);
        });
      return;
    }

    //!want [form] <pokemon>
    db.getPokemonByName(args[args.length-1])
      .then((pokemon) => {
        if (pokemon === undefined) {
          message.channel.send('Cha!🎶 Ive never heard of that pokemon.');
          return;
        }

        var addRole = function(role) {
          message.member.addRole(role)
            .then((guildMember) => {
              message.channel.send('🎶Cha TOT!🎶 '+guildMember.user+' now wants ' + pokemon.name);
            })
            .catch((err) => {
              sendError(message, err);
            });
        }

        var role = message.guild.roles.find(val => val.name === pokemon.name);

        if (!role) {
          createRole(message, pokemon.name)
            .then((role) => {
              addRole(role);
            })
            .catch((err) => {
              sendError(message, err);
            });
        } else {
          addRole(role);
        }
      })
      .catch((err) => {
        sendError(message, err);
      });
  }

  //!unwant [form] <pokemon>
  this.unwant = function(message, args) {
    if (args.length < 1) { this.help(message, 'unwant'); return; }
    db.getPokemonByName(args[args.length-1])
      .then((pokemon) => {
        if (typeof pokemon === 'undefined') {
          message.channel.send('Cha!🎶 Ive never heard of that pokemon.');
          return;
        }

        var role = message.guild.roles.find(val => val.name === pokemon.name);


        if (role) {
          message.member.removeRole(role)
            .then((guildMember) => {
              message.channel.send('🎶 Cha! '+guildMember.user+' no longer wants '+pokemon.name);    
            })
            .catch((err) => {
              sendError(message, err);
            });
        } else {
          message.channel.send('🎶 Cha! '+message.author+' doesn\'t want '+pokemon.name);    
        }
      })
      .catch((err) => {
        sendError(message, err);
      });
  }

  //help [command]
  this.help = function(message, args) {
    if (typeof args === 'string') args = [args];
    if (args.length < 1) {
      this.help(message, 'help');
      return;
    }

    if (!helpMsgs.hasOwnProperty(args[0])) {
      message.channel.send('Tot.. I don\'t recognize that command. Try !help');
      return;
    }

    message.channel.send(helpMsgs[args[0]]);
  }

  function roleExists(message, role) {
    return message.guild.roles.find('name', role) !== undefined;
  }

  async function createRole(message, role) {
    return await message.guild.createRole({ name: role });
  }

  function sendError(message, err) {
    message.channel.send('🎶 cha.. I had a problem doing your request '+err.message);
    //sendMessage(channelID, '🎶 cha.. I had a problem doing your request ');
  }


  return this; 
}

module.exports = Commands;
