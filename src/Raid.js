var moment = require('moment-timezone');
moment.tz.setDefault('America/Chicago');


var Raid = function(message, type, timeLeft, level, pokemon) {
  
  this.type = type;
  this.author = message.author.toString();
  this.channelID = message.channel.id;
  this.guildID = message.guild.id;
  this.timeLeft = timeLeft;
  this.timeCreated = moment(message.createdAt);
  this.timeEnd = moment(this.timeCreated).add(timeLeft, 'minutes');
  this.timeBegan = moment(this.timeEnd).add(-Raid.DURATION, 'minutes');
  this.nextGroupTime = moment(this.timeEnd).add(-Raid.TIME_BEFORE_END, 'minutes');
  this.level = level;
  this.pokemon = pokemon;
  this.coming = new Map();

  var timeout = null;
  var groupTimeout = null;

  if (this.pokemon !== undefined) {
    this.role = message.guild.roles.find(val => val.name === pokemon.name);
  }

  this.hasGroup = function() {
    return this.nextGroupTime.isBefore(this.timeEnd);
  }

  this.fGroupTime = function() {
    return fTime(this.nextGroupTime);
  }

  this.fTimeEnd = function() {
    return fTime(this.timeEnd);
  }

  this.onEnd = function(fn) {
    if (moment().isSameOrAfter(this.timeEnd)) return;
    clearTimeout(timeout);
    timeout = setTimeout(fn, this.timeLeft*60*1000);
  }

  this.onGroupTime = function(fn) {
    if (moment().isSameOrAfter(this.nextGroupTime)) return;
    clearTimeout(groupTimeout);
    groupTimeout = setTimeout(fn, (this.timeLeft - Raid.TIME_BEFORE_END)*60*1000);
  }

  this.clearTimeouts = function() {
    clearTimeout(groupTimeout);
    clearTimeout(timeout); 
  }

  this.getRoleMention = function() {
    if (this.role !== undefined && this.role !== null) {
      return this.role.toString();
    }
    return '';
  }

  this.addComing = function(user) {
    this.coming.set(user.id, user.toString());
  }

  this.removeComing = function(user) {
    this.coming.delete(user.id);
  }

  this.getComingMentions = function() {
    var comingStr = '';
    this.coming.forEach((val) => {
      comingStr += val + ' ';
    });
    return comingStr;
  }

  function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60*1000);
  }

  function fTime(date) {
   return date.format('LT');
  }



  return this;
}

//Raid Types
Raid.EGG = 'EGG';
Raid.POKEMON = 'POKEMON';
Raid.TIME_BEFORE_END = 15;
Raid.DURATION = 45;

module.exports = Raid;
