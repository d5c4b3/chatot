const { Client } = require('discord.js');
var logger = require('winston');
var auth = require('../auth.json');
var Commands = require('./Commands.js');

/* How I'm doing Objects
var ClassName = function() {
  // Private variable
  var my_variable = 5;
  // Public variabl
  this.my_variable = 5;

  //Private function
  function foo() {

  }

  //Pubic
  this.foo = function() {

  }
}
*/

var Bot = function() {

  var cmds;

  /* PUBLIC */
  this.start = function() {
    /*========== LOGGER ==========*/
    logger.remove(logger.transports.Console);
    logger.add(new logger.transports.Console, {
      colorize: true,
      format: logger.format.simple()
    });

    /*========== DISCORD BOT ==========*/
    var client = new Client();

    cmds = new Commands(client, logger);

    client.on('ready', function(evt) {
      logger.info('Connected');
    });

    client.on('message', (message) => {
      if (!message.guild || message.author.bot) return;
      //Handle Commands
      // only look for commands starting with !
      if (message.content.substring(0,1) == '!') {
        var args = message.content.substring(1).split(' ');
        var cmd = args[0]; //The first word is the command
        args.shift(); //Remove the command from the parameters

        //If the Command.js object does not have the command as a method
        if (typeof cmds[cmd] !== 'function') {
          message.channel.send('🎶Cha🎶 I\'m sorry I don\'t know what you mean.');
          return;
        }

        //Run the Command.js method corresponding to the command we were given
        cmds[cmd](message, args);
      }
    });

    client.login(auth.token);
  }

  /* PRIVATE */

  return this;
}

module.exports = Bot;
