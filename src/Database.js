var auth = require('../auth.json');
var mariadb = require('mariadb');

var pool = null;
var cache = {};

var Database = function(logger) {

  this.filterPokeRoles = async function(roles) {
    var queryStr = 'SELECT name FROM pokedex WHERE name in (';
    queryStr += Array(roles.length).fill('?').join(',');
    queryStr += ')';
    return await removeMeta(query(queryStr, roles));
  }

  this.addRaidChannel = async function(guild_id, channel_id) {
    var queryStr = 'INSERT IGNORE INTO raid_channels VALUES (?, ?)';
    return await query(queryStr, [guild_id, channel_id]);
  }

  this.isRaidChannel = async function(channel_id) {
    var queryStr = 'SELECT 1 AS bool FROM raid_channels WHERE channel_id = ?';
    return await returnBool(query(queryStr, [channel_id]));
  }

  /*
  this.getUserWants = async function (userID) {
    var queryStr = 'SELECT name, id, form FROM wants JOIN pokedex ON id = pokedex_num WHERE user_id = ?';
    return await removeMeta(query(queryStr, [userID]));
  }

  this.addUserWants = async function(userID, pokemon, form) {
    if (typeof form === 'undefined') form = null;
    var queryStr = 'INSERT IGNORE INTO wants VALUES (?, ?, ?)';
    return await query(queryStr, [userID, pokemon, form]);
  }

  this.removeUserWants = async function(userID, pokemon) {
    var queryStr = 'DELETE FROM wants WHERE user_id = ? AND pokedex_num = ?';
    return await query(queryStr, [userID, pokemon]);
  }
  */

  this.getPokemonByName = async function(name) {
    name = name.toUpperCase();
    var ns = 'poke_by_name';

    if (hasCache(name, ns)) return getCache(name, ns);

    var queryStr = 'SELECT name, id FROM pokedex WHERE UPPER(name) = ?';
    return await new Promise((resolve, reject) => {
      returnOne(query(queryStr, [name])).then((data) => {
        if (typeof data === 'undefined') resolve(data);
        setCache(data.name.toUpperCase(), data, ns);

        resolve(data);
      }).catch((err) => {
        reject(err);
      });
    })
  }

  function hasCacheNS(ns) {
    return cache.hasOwnProperty(ns);
  }

  function hasCache(key, ns) {
    if (!hasCacheNS(ns)) return false;
    return cache[ns].hasOwnProperty(key);
  }

  function setCache(key, val, ns) {
    if (!hasCacheNS(ns)) cache[ns] = {};
    cache[ns][key] = val;
  }

  function getCache(key, ns) {
    if (!hasCacheNS(ns)) return undefined;
    return cache[ns][key];
  }

  async function removeMeta(val) {
    return new Promise((resolve, reject) => {
      val.then((data) => {
        delete data['meta'];
        resolve(data);
      }).catch((err) => {
        reject(err);
      })
    });
  }

  async function returnOne(val) {
    return new Promise((resolve, reject) => {
      val.then((data) => {
        delete data['meta'];
        resolve(data[0]);
      }).catch((err) => {
        reject(err);
      })
    });
  }

  async function returnBool(val) {
    return new Promise((resolve, reject) => {
      val.then((data) => {
        delete data['meta'];
        resolve(data[0]['bool'] == '1');
      }).catch((err) => {
        reject(err);
      })
    });
  }

  async function getConn() {
    if (pool === null) {
      pool = mariadb.createPool({host: auth.db.host, port: auth.db.port, user:auth.db.user, password:auth.db.pass, database:auth.db.database, connectionLimit: 5});
    }

    return pool.getConnection();
  }

  async function query(queryStr, params) {
    conn = await getConn();
    try {
      if (!params) {
        return await conn.query(queryStr);
      } else {
        return await conn.query(queryStr, params);
      }
    } catch (err) {
      throw err;
    } finally {
      if (conn) conn.end();
    }
  }

  return this;
}

module.exports = Database;
